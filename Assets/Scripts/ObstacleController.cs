﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleController : MonoBehaviour {
	public float vel_const;
	private float vel;
	public enum Types { Neutral, Penalty, Friend };
	public Types type;
	public Vector2 obstPos;
	private GameObject globals;
	public bool isVisible;

	// Use this for initialization
	void Start () {
		isVisible = false;
		// global object
		globals = GameObject.FindWithTag("Global");
		// TODO adjust obstacle speed curve
		vel = (vel_const + Random.Range(0,0.05f))* globals.GetComponent<GlobalsController>().level;
		obstPos = new Vector2();
		type = (Types)Random.Range(0, 1);

		PickLane();
	}
	
	// Update is called once per frame
	void Update () {
		MoveOut();
	}

	/// <summary>
	/// OnBecameInvisible is called when the renderer is no longer visible by any camera.
	/// </summary>
	void OnBecameInvisible()
	{
		if(isVisible){
			CashScore();
			Debug.Log(globals.GetComponent<GlobalsController>().score);
			Destroy(gameObject);
		}
	}

	/// <summary>
	/// OnBecameVisible is called when the renderer became visible by any camera.
	/// </summary>
	void OnBecameVisible()
	{
		isVisible = true;
	}

	/// <summary>
	/// Sent when an incoming collider makes contact with this object's
	/// collider (2D physics only).
	/// </summary>
	/// <param name="other">The Collision2D data associated with this collision.</param>
	void OnCollisionEnter2D(Collision2D other)
	{
		if(type == Types.Friend) {
			CashPool();
		}
	}

	void MoveOut(){
		transform.Translate(Vector2.left * vel);
	}
	void CashScore(){
		if(type == Types.Penalty ){
			globals.GetComponent<GlobalsController>().score += 100 * globals.GetComponent<GlobalsController>().multiplier;
		} else if (type == Types.Friend) {
			globals.GetComponent<GlobalsController>().scorePool += 100 * globals.GetComponent<GlobalsController>().multiplier;
			globals.GetComponent<GlobalsController>().multiplier++;
		}
	}

	void CashPool(){
		globals.GetComponent<GlobalsController>().score += globals.GetComponent<GlobalsController>().scorePool * globals.GetComponent<GlobalsController>().multiplier;
		globals.GetComponent<GlobalsController>().scorePool = 0;
	}

	void RunPenalty(){
		// fazer o jogador ficar parado pelo tempo da penalidade
		globals.GetComponent<GlobalsController>().multiplier = 1;
	}

	void PickLane(int lane = 0){
		// escolher uma trilha para seguir
	}
}
