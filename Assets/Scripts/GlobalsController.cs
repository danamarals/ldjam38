﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalsController : MonoBehaviour {

	public float timer_prazo;
	public float timer_chegada;
	public float timerSpeed;
	public int score, scorePool, level, multiplier;
	//public int maxSpawn;
	public bool gameState = false;

	// Use this for initialization
	void Start () {
		// for demonstration purposes
		//level = (int)Random.Range(1, 5);
		level = 1;
		score = 0;
		scorePool = 0;
		multiplier = 1;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	/// <summary>
	/// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
	/// </summary>
	void FixedUpdate()
	{
		timer_prazo -= timerSpeed;
		timer_chegada += timerSpeed;
	}

	// starts match
	void StartGame(){
		if(gameState == false){
			gameState = true;
			level = 1;
		}
	}

	// TODO implement quit match
	void QuitGame(){
		if(gameState == true) {
			gameState = false;
			level = 1;
		}
		if(timer_prazo == 0.0f){
			gameState = false;
		}
	}
}
