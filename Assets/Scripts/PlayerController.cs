﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public bool hasCollided = false;
	public int currentLane = 1;
	public Vector2 PlayerPosition;

	// Use this for initialization
	void Start () {
		//currentLane = GlobalController.Lanes/2;
		PlayerPosition = new Vector2(-1f, 0);
	}
	
	// Update is called once per frame
	void Update () {
		/*
		 * Lembrar de implementar o swipe para mobile
		 */
		if(Input.GetKeyDown(KeyCode.DownArrow)){
			if(currentLane > 0)
				currentLane--;
		} else if(Input.GetKeyDown(KeyCode.UpArrow)){
			if(currentLane < 2)
				currentLane++;
		}
		MoveToLane();
	}

	private void MoveToLane(){
		switch (currentLane)
		{
			case 0:
				PlayerPosition = new Vector2(-1f, -.25f);
				break;
			case 1:
				PlayerPosition = new Vector2(-1f, 0);
				break;
			case 2:
				PlayerPosition = new Vector2(-1f, .25f);
				break;
		}
		transform.position = PlayerPosition;
	}
	
	/// <summary>
	/// Sent when an incoming collider makes contact with this object's
	/// collider (2D physics only).
	/// </summary>
	/// <param name="other">The Collision2D data associated with this collision.</param>
	void OnCollisionEnter2D(Collision2D other)
	{
		/// in case of colliding with an obstacle, deduce total time
	}
}
