﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerScript : MonoBehaviour {

	public GameObject spawn;
	public float spawnTimer;
	public float time;

	// Use this for initialization
	void Start () {
		setTimer();
	}
	
	// Update is called once per frame
	void Update () {
		spawnTimer += Time.deltaTime;
	}

	/// <summary>
	/// This function is called every fixed framerate frame, if the MonoBehaviour is enabled.
	/// </summary>
	void FixedUpdate()
	{
		// calling mob
		if(spawnTimer >= time){
			spawnObstacle();
		}
	}

	void spawnObstacle(){
		Instantiate(spawn, transform.position, transform.rotation);
		setTimer();
	}
	// set timer for 
	void setTimer(){
		time = (float)Random.Range(1.5f, 2.5f);
		spawnTimer = 0f;
	}
}
